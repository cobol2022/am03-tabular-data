       IDENTIFICATION DIVISION. 
       PROGRAM-ID. OX.
       AUTHOR. PLAIIFAH.

       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  WS-ROW OCCURS 3 TIMES .
           05 WS-COL   PIC X OCCURS 3 TIMES VALUE "-".

       01  WS-IDX-ROW PIC 9.
       01  WS-IDX-COL PIC 9.
       01  WS-INPUT-ROW PIC 9.
           88 WS-INPUT-ROW-VALID VALUE 1 THRU 3.
       01  WS-INPUT-COL PIC 9.
           88 WS-INPUT-COL-VALID VALUE 1 THRU 3.
       01  WS-COUNT PIC 9(2) VALUE ZEROS.
       01  WS-PLAYER PIC X VALUE "X".

       01  WS-PLAYER-CHECK PIC X VALUE 1.
       01  WS-ROW-CHECK   PIC 9.
       01  WS-COL-CHECK   PIC 9.

       01  CHECKER PIC X.
       01  CHECKER-X2 PIC 9.


       PROCEDURE DIVISION.
       BEGIN.
           PERFORM UNTIL WS-COUNT>9
              PERFORM DISPLAY-TURN
              PERFORM INPUT-ROW-COL
              PERFORM PUT-TABLE
              PERFORM DISPLAY-TABLE
              IF WS-COUNT > 8
                 DISPLAY "DRAW"
                 STOP RUN
              END-IF 
              PERFORM CHECK-WIN
              PERFORM TURN-PLAYER 
              PERFORM RESET-ROW-COL
           END-PERFORM

           GOBACK.

       PUT-TABLE.
           IF WS-COL(WS-INPUT-ROW, WS-INPUT-COL) = "-"
              MOVE WS-PLAYER TO WS-COL(WS-INPUT-ROW, WS-INPUT-COL)
              ADD 1 TO WS-COUNT
              MOVE WS-COL(WS-INPUT-ROW, WS-INPUT-COL) TO WS-PLAYER-CHECK
              COMPUTE WS-ROW-CHECK = WS-INPUT-ROW
              COMPUTE WS-COL-CHECK = WS-INPUT-COL 
           ELSE
              DISPLAY "ERROR"
           END-IF .

       DISPLAY-TURN.
           DISPLAY "TURN " WS-PLAYER.

       TURN-PLAYER.
           IF WS-PLAYER = "X"
              MOVE "O" TO WS-PLAYER
           ELSE
              MOVE "X" TO WS-PLAYER
           END-IF.

       RESET-ROW-COL.
           MOVE ZEROS TO WS-INPUT-ROW, WS-INPUT-COL.

       INPUT-ROW-COL.
           PERFORM UNTIL WS-INPUT-ROW-VALID    
              DISPLAY "INPUT ROW -"
              ACCEPT WS-INPUT-ROW 
           END-PERFORM
           PERFORM UNTIL WS-INPUT-COL-VALID
              DISPLAY "INPUT COL -"
              ACCEPT WS-INPUT-COL 
           END-PERFORM.

       DISPLAY-TABLE.
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              PERFORM VARYING WS-IDX-COL FROM 1 BY 1
                 UNTIL WS-IDX-COL > 3
                 DISPLAY WS-COL(WS-IDX-ROW, WS-IDX-COL)
                    WITH NO ADVANCING 
              END-PERFORM
              DISPLAY " "
           END-PERFORM.

       CHECK-WIN.
           PERFORM CHECK-ROW
           PERFORM CHECK-COL
           PERFORM CHECK-X2
           PERFORM CHECK-X1.

       CHECK-ROW.
           MOVE 1 TO CHECKER
           PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3
              IF WS-COL(WS-ROW-CHECK, WS-IDX-COL)
                      IS NOT EQUAL TO WS-PLAYER-CHECK 
                    MOVE 0 TO CHECKER
              END-IF 
           END-PERFORM.
      *    DISPLAY CHECKER "--ROW"
           IF CHECKER IS EQUAL TO  1
              PERFORM DISPLAY-WIN
           STOP RUN
           END-IF.

       CHECK-COL.
           MOVE 1 TO CHECKER
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
              IF WS-COL(WS-IDX-ROW, WS-COL-CHECK)
                      IS NOT EQUAL TO WS-PLAYER-CHECK 
                    MOVE 0 TO CHECKER
              END-IF 
           END-PERFORM.
      *    DISPLAY CHECKER "--COL"
           IF CHECKER IS EQUAL TO  1
              PERFORM DISPLAY-WIN
              STOP RUN
           END-IF.
           
       CHECK-X1.
           MOVE 1 TO CHECKER
           PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3
              IF WS-COL(WS-IDX-COL, WS-IDX-COL)
                      IS NOT EQUAL TO WS-PLAYER-CHECK 
                    MOVE 0 TO CHECKER
              END-IF 
           END-PERFORM.
           IF CHECKER IS EQUAL TO  1
              PERFORM DISPLAY-WIN
           STOP RUN
           END-IF.

       CHECK-X2.
           MOVE 1 TO CHECKER
           MOVE 3 TO CHECKER-X2
           PERFORM VARYING WS-IDX-COL FROM 1 BY 1
              UNTIL WS-IDX-COL > 3
              IF WS-COL(WS-IDX-COL, CHECKER-X2)
                      IS NOT EQUAL TO WS-PLAYER-CHECK 
                    MOVE 0 TO CHECKER
              END-IF 
              COMPUTE CHECKER-X2 = CHECKER-X2 - 1
           END-PERFORM.
           IF CHECKER IS EQUAL TO  1
              PERFORM DISPLAY-WIN
           STOP RUN
           END-IF.

       DISPLAY-WIN.
           DISPLAY WS-PLAYER " WIN".


       

       
